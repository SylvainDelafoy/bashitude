#!/usr/bin/env bats

function fizzBuzz {
	for NUMBER in $( seq 1 $1 ); do
		local WORD=""
		if [ $((NUMBER % 3)) -eq 0 ] ; then
			WORD=$WORD"Fizz"
		fi
		if [ $((NUMBER % 5 )) -eq 0 ] ; then
			WORD=$WORD"Buzz"
		fi
		if [ -z $WORD ] ; then
			WORD=$NUMBER;
		fi
		echo $WORD
	done;
}


@test "Prints 1" {
  result="$(fizzBuzz 1 )"
  [ "$result" -eq 1 ]
}



@test "Prints 2" {
  result="$(fizzBuzz 2 )"
  [ "$result" == $'1\n2'  ]
}


@test "Prints Fizz" {
  result="$(fizzBuzz 3 )"
  [ "$result" == $'1\n2\nFizz'  ]
}

@test "Prints Buzz" {
  result="$(fizzBuzz 5 )"
  echo "$result" | grep Buzz$
}

@test "Prints FizzBuzz" {
  result="$(fizzBuzz 15 )"
  echo "$result" | grep FizzBuzz$
}

