# Shell experiments

## "install" the test framework
```shell
PATH=$PATH:/g/Data/git/bats/libexec/
```

## References
* [test framework](https://github.com/sstephenson/bats)
* [bash beginers guide:](http://www.tldp.org/LDP/Bash-Beginners-Guide/html/index.html)
